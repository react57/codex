import Button from "../components/Button";

function SearchBar() {
  return (
    <div className="w-full">
      <div className="flex">
        <input
          type="text"
          placeholder="Search for what your're looking for"
          className="w-full appearance-none bg-white border-gray-300 rounded-md border-solid border box-border cursor-text block text-base leading-normal m-0 py-3 px-4 shadow focus:border-blue-200"
        />
        <Button text="Search" className="ml-3" />
      </div>
    </div>
  );
}

export default SearchBar;
