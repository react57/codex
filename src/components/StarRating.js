function StarRating({ rating }) {
  function getStar(color, key) {
    return (
      <svg
        key={key}
        className={`mx-1 w-4 h-4 fill-current text-${color}-400`}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 20 20"
      >
        <path d="M10 15l-5.878 3.09 1.123-6.545L.489 6.91l6.572-.955L10 0l2.939 5.955 6.572.955-4.756 4.635 1.123 6.545z" />
      </svg>
    );
  }

  const starList = [];

  for (let i = 1; i < 6; i++) {
    let color = rating >= i ? "yellow" : "gray";
    starList.push(getStar(color, i));
  }

  return (
    <div className="flex">
      <div className="flex items-center mt-2 mb-4">{starList}</div>
    </div>
  );
}

export default StarRating;
