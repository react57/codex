import Navbar from "./Navigation/Navbar";
import Container from "./Container";

function Layout(props) {
  return (
    <>
      <Navbar></Navbar>
      <Container>{props.children}</Container>
    </>
  );
}

export default Layout;
