function Button({ className, text, onClickHandler = () => {} }) {
  const classNames = ["inline-flex rounded-md shadow", className].join(" ");
  return (
    <div className={classNames}>
      <button
        className="px-5 py-3 border border-transparent text-base font-medium rounded-md text-white bg-gray-800 hover:bg-gray-700"
        onClick={onClickHandler}
      >
        {text}
      </button>
    </div>
  );
}

export default Button;
