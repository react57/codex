function Container(props) {
  return (
    // <div className="max-w-7xl py-6 sm:px-6 lg:px-8 mt-20">
    //   <div className="fixed w-screen py-6 sm:px-0 mt-10">{props.children}</div>
    // </div>
    <div className="py-6 sm:px-6 lg:px-8 mt-20">{props.children}</div>
  );
}

export default Container;
