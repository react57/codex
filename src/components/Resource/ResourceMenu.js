import { Link } from "react-router-dom";

function ResourceMenu({ category }) {
  return (
    <li className="mt-8">
      <h5 className="px-3 mb-3 lg:mb-3 uppercase tracking-wide font-bold text-sm lg:text-xs text-gray-900">
        {category.title}
      </h5>
      <ul>
        {category.techs.length > 0 ? (
          category.techs.map((item) => (
            <li key={item._id}>
              <Link
                to={`/resources/${item._id}`}
                className="px-3 py-2 transition-colors duration-200 relative block hover:text-gray-900 text-gray-500"
                href="/docs/installation"
              >
                <span className="rounded-md absolute inset-0 bg-cyan-50 opacity-0"></span>
                <span className="relative">{item.title}</span>
              </Link>
            </li>
          ))
        ) : (
          <li>
            <span className="relative">None</span>
          </li>
        )}
      </ul>
    </li>
  );
}

export default ResourceMenu;
