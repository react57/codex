import Badge from "../Badge";
import StarRating from "../StarRating";

function ResourceItem({ link }) {
  return (
    <div className="mx-1 mb-10">
      <div className="max-w-6xl px-3 lg:px-10 py-6 bg-white rounded-lg border">
        <div className="flex justify-between items-center"></div>
        <div className="mt-2">
          <a
            href="#"
            className="text-2xl text-gray-700 font-bold hover:underline"
          >
            {link.title}
          </a>
          <p className="mt-2 text-gray-600">{link.description}</p>
        </div>
        <div className="flex justify-start mt-3">
          {link.tags.map((tag, index) => (
            <Badge key={index} text={tag} />
          ))}
        </div>
        <StarRating rating={2} />
        <div className="flex justify-between items-center mt-4">
          <a href="#" className="text-blue-500 hover:underline">
            Official site
          </a>
        </div>
      </div>
    </div>
  );
}

export default ResourceItem;
