import { useEffect, useState } from "react";
import axios from "axios";
import MobileSidebar from "../Navigation/MobileSidebar";
import Sidebar from "../Navigation/Sidebar";
import SideNavToggle from "../Navigation/SideNavToggle";

const fetchCategories = async (setter) => {
  try {
    const result = await axios.get("http://localhost:5000/api/category");
    setter(result.data);
  } catch (error) {
    // @todo do something else with this error
    console.log(error);
  }
};

function ResourceLayout(props) {
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    // @todo refactor to a hook - reference Done with it repo useApi() hook
    fetchCategories(setCategories);
  }, []);

  return (
    <div className="w-full mx-auto pt-0 mt-0">
      <div className="flex">
        {/* <MobileSidebar /> */}
        <Sidebar categories={categories} />
        {props.children}
      </div>
      <SideNavToggle />
    </div>
  );
}

export default ResourceLayout;
