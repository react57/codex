import { useState, useEffect } from "react";
import { useParams, useLocation } from "react-router-dom";
import ResourceHeader from "./ResourceHeader";
import SideNavToggle from "../Navigation/SideNavToggle";
import MobileSidebar from "../Navigation/MobileSidebar";
import Sidebar from "../Navigation/Sidebar";
import ResourceItem from "./ResourceItem";
import ResourceLayout from "./ResourceLayout";
import SearchBar from "../SearchBar";

function ResourceSearchPage() {
  return (
    <ResourceLayout>
      <div className="flex-1 px-0 lg:px-32">
        <div className="mb-2 lg:mb-10 max-w-6xl">
          <ResourceHeader />
          <div className="pt-8">
            <SearchBar />
          </div>
        </div>
        <div></div>
      </div>
    </ResourceLayout>
  );
}

export default ResourceSearchPage;
