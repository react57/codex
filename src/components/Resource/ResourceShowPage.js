import { useState, useEffect } from "react";
import { useParams, useLocation } from "react-router-dom";
import axios from "axios";
import ResourceHeader from "./ResourceHeader";
import SideNavToggle from "../Navigation/SideNavToggle";
import MobileSidebar from "../Navigation/MobileSidebar";
import Sidebar from "../Navigation/Sidebar";
import ResourceItem from "./ResourceItem";
import ResourceLayout from "./ResourceLayout";

function ResourceShowPage() {
  const [tech, setTech] = useState(null);
  const { techId } = useParams();
  const location = useLocation();

  const fetchTech = async (setter) => {
    try {
      const result = await axios.get(
        `http://localhost:5000/api/tech/${techId}`
      );
      setter(result.data);
    } catch (error) {
      // @todo do something else with this error
      console.log(error);
    }
  };

  useEffect(() => {
    fetchTech(setTech);
  }, [location]);

  const linkList = <span></span>;

  return (
    // @todo use isLoading state instead
    tech && (
      <ResourceLayout>
        <div className="flex-1 px-0 lg:px-32">
          <div className="mb-2 lg:mb-10 max-w-6xl">
            <ResourceHeader title={tech.title} description={tech.description} />
          </div>
          <div>
            {tech.links.map((item) => (
              <ResourceItem key={item._id} link={item} />
            ))}
          </div>
        </div>
      </ResourceLayout>
    )
  );
}

export default ResourceShowPage;
