function ResourceHeader({
  title = "Resources",
  description = "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Temporexpedita dicta totam aspernatur doloremque",
}) {
  return (
    <>
      <div className="px-2 lg:flex lg:items-center lg:justify-between">
        <div className="flex-1 min-w-0">
          <h2 className="pb-2 text-4xl font-bold leading-7 text-gray-900">
            {title}
          </h2>
          <p className="mt-2 text-gray-600 max-w-3xl">{description}</p>
        </div>
      </div>
    </>
  );
}

export default ResourceHeader;
