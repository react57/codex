function Badge({ text }) {
  return (
    <span className="text-xs font-semibold inline-block py-1 px-2 rounded text-gray-400 bg-gray-100 uppercase last:mr-0 mr-1">
      {text}
    </span>
  );
}

export default Badge;
