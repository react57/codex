import ResourceMenu from "../Resource/ResourceMenu";

function Sidebar({ categories }) {
  return (
    <div className="top-0 h-screen px-5 pt-10 mb-100 sticky overflow-y-scroll lg:block hidden">
      {categories.length > 0 ? (
        <ul className="lg:pb-20 px-0">
          {categories.map((item) => {
            return <ResourceMenu key={item._id} category={item} />;
          })}
        </ul>
      ) : (
        <h5 className="px-3 mb-3 lg:mb-3 uppercase tracking-wide font-bold text-sm lg:text-xs text-gray-900">
          No data
        </h5>
      )}
    </div>
  );
}

export default Sidebar;
