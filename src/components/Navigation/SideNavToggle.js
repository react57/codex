function SideNavToggle() {
  const toggleSidebarNav = function () {
    const sidebar = document.querySelector("#mobileSidebar");
    sidebar.classList.toggle("hidden");
  };
  return (
    <button
      onClick={toggleSidebarNav}
      type="button"
      className="fixed z-50 bottom-4 right-8 w-16 h-16 rounded-full bg-gray-900 text-white block lg:hidden"
    >
      <span className="sr-only">Open site navigation</span>
      <svg
        width="24"
        height="24"
        fill="none"
        className="absolute top-1/2 left-1/2 -mt-3 -ml-3 transition duration-300 transform"
      >
        <path
          d="M4 8h16M4 16h16"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        ></path>
      </svg>
      <svg
        width="24"
        height="24"
        fill="none"
        className="absolute top-1/2 left-1/2 -mt-3 -ml-3 transition duration-300 transform opacity-0 scale-80"
      >
        <path
          d="M6 18L18 6M6 6l12 12"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        ></path>
      </svg>
    </button>
  );
}

export default SideNavToggle;
