import { useState } from "react";
import { Link } from "react-router-dom";

function NavItem({ href = "/", text, isActive = false, DropdownMenu = null }) {
  const [dropdownShown, setDropdownShown] = useState(false);

  const toggleDropdownShown = () => setDropdownShown(!dropdownShown);

  const itemStyles = isActive
    ? "bg-gray-900 text-white px-3 py-2 rounded-md text-sm font-medium"
    : "text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium";

  return DropdownMenu ? (
    <div className="relative">
      <button className={itemStyles} onClick={toggleDropdownShown}>
        {text}
      </button>
      <DropdownMenu isShown={dropdownShown}></DropdownMenu>
    </div>
  ) : (
    <Link to={href} className={itemStyles}>
      {text}
    </Link>
  );
}

export default NavItem;
