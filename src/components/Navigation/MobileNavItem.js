import { Link } from "react-router-dom";

function MobileNavItem({ href = "/", text, isActive = false }) {
  const itemStyles = isActive
    ? "bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium"
    : "text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium";

  return (
    <Link to={href} className={itemStyles}>
      {text}
    </Link>
  );
}

export default MobileNavItem;
