import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Layout from "./components/Layout";
import Home from "./components/Home";
import ResourceSearchPage from "./components/Resource/ResourceSearchPage";
import ResourceShowPage from "./components/Resource/ResourceShowPage";

function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/resources" exact>
            <ResourceSearchPage />
          </Route>
          <Route path="/resources/:techId?">
            <ResourceShowPage />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
